package com.game.of.riches.model.users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class ModelUsers{
  @SerializedName("pagination")
  @Expose
  private Pagination pagination;
  @SerializedName("data")
  @Expose
  private List<Data> data;
  @SerializedName("status")
  @Expose
  private Status status;
  public void setPagination(Pagination pagination){
   this.pagination=pagination;
  }
  public Pagination getPagination(){
   return pagination;
  }
  public void setData(List<Data> data){
   this.data=data;
  }
  public List<Data> getData(){
   return data;
  }
  public void setStatus(Status status){
   this.status=status;
  }
  public Status getStatus(){
   return status;
  }
}