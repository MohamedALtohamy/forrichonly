package com.game.of.riches;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.game.of.riches.dialog.DialogInfoApp;
import com.sdsmdg.tastytoast.TastyToast;
import java.util.List;

public class TestInApp extends AppCompatActivity implements PurchasesUpdatedListener {

  private BillingClient mBillingClient;
  //String productId = "android.test.purchased";
  String productId = "50";

  // String productId="0";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_test_in_app);
    isServiceInAppAvailable();


  }

  private void isServiceInAppAvailable() {
    mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
    mBillingClient.startConnection(new BillingClientStateListener() {
      @Override
      public void onBillingSetupFinished(@BillingResponse int billingResponseCode) {
        if (billingResponseCode == BillingResponse.OK) {
          BillingFlowParams flowParams = BillingFlowParams.newBuilder()
              .setSku(productId)
              .setType(SkuType.INAPP) // SkuType.SUB for subscription
              .build();
          int responseCode = mBillingClient.launchBillingFlow(TestInApp.this, flowParams);
          Log.e("code", responseCode + "");
        }
      }

      @Override
      public void onBillingServiceDisconnected() {
        // Try to restart the connection on the next request to
        // Google Play by calling the startConnection() method.
      }
    });
  }

  @Override
  public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
    Log.e("ggg", "fffkf");
    if (responseCode == BillingResponse.OK
        && purchases != null) {
      for (Purchase purchase : purchases) {
        handlePurchase(purchase);
      }
    } else if (responseCode == BillingResponse.USER_CANCELED) {
      DialogInfoApp.showDialogInfo(this, "cancelled");
      // Handle an error caused by a user cancelling the purchase flow.
    } else {
      DialogInfoApp.showDialogInfo(this, "rrt");
      // Handle any other error codes.
    }
  }

  private void handlePurchase(Purchase purchase) {
    TastyToast.makeText(this, purchase.getOriginalJson(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
    DialogInfoApp.showDialogInfo(this, purchase.getOriginalJson());

  }
}
