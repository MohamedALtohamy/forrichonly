package com.game.of.riches.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.game.of.riches.R;
import com.game.of.riches.dialog.DialogInfoApp;
import com.game.of.riches.interfaces.HandleRetrofitResp;
import com.game.of.riches.model.forgetpwd.ModelForgetPassword;
import com.game.of.riches.model.profile.Data;
import com.game.of.riches.model.profile.Profile;
import com.game.of.riches.retrofitconfig.HandelCalls;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.sdsmdg.tastytoast.TastyToast;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends BaseActivity implements Validator.ValidationListener, HandleRetrofitResp {
//https://www.flaticon.com
    //https://github.com/hbb20/CountryCodePickerProject

    private Toolbar mToolbar;

    @Email
    private EditText mLoginEmail;
    @NotEmpty
    private EditText mLoginPassword;

    private Button mLogin_btn;
    private TextView tvForgetPwd;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initToolBar();

        initView();
        initValidate();
        initAddMob();
        //  mLoginEmail.setText("moh@gmail.com");
        //  mLoginPassword.setText("123123");
        /*
        BraintreeGateway gateway = new BraintreeGateway(
                Environment.PRODUCTION,
                "tmhzdq5v7q5bxbsr",
                "yrq2gggmk2jjp8th",
                "b4dbee3e538e00e7e91935009891b855"
        );
        */
        //


    }
    @Override
    protected void attachBaseContext(Context cnt) {
        super.attachBaseContext(LocaleHelper.onAttach(cnt));
        Log.e("onbase","base0");
    }

    private void initValidate() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initView() {
        mLoginEmail = (EditText) findViewById(R.id.login_email);
        mLoginPassword = (EditText) findViewById(R.id.login_password);
        mLogin_btn = (Button) findViewById(R.id.login_btn);
        tvForgetPwd= (TextView) findViewById(R.id.tvForgetPwdchat);

        tvForgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("fr","frfff");
               DialogInfoApp.initDialogForgetPwd(LoginActivity.this);

            }
        });
        findViewById(R.id.tvDescreption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInfoApp.showDialogInfo(LoginActivity.this,getString(R.string.info_app));

            }
        });


        mLogin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();


            }
        });
    }
    private void initAddMob() {
        AdView mAdView = findViewById(R.id.adView);

        HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_login));
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.login_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.login));
    }


    public void callResetPwd(String email) {
        HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("email",email);
        HandelCalls.getInstance(this).call(DataEnum.call_forget_pwd.name(),meMap, true, this);

    }

    @Override
    public void onValidationSucceeded() {
        String email = mLoginEmail.getText().toString();
        String password = mLoginPassword.getText().toString();

        // loginUser(email, password);

        HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("email", email);
        meMap.put("password", password);

        HandelCalls.getInstance(LoginActivity.this).call(DataEnum.LoginApiCall.name(), meMap, true, this);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        //==========================================//
        if(flag.equals(DataEnum.LoginApiCall.name())){
            Profile profile = (Profile) o;
            if (profile.getStatus().getStatus()) {
                profile.getData().setEmail(mLoginEmail.getText().toString());
                PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();
                Log.e("token", profile.getData().getToken());
                PrefsUtil.with(this).add(DataEnum.AuthToken.name(), profile.getData().getToken()).apply();

                Data s = (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(), Data.class);
                Log.e("my nmae", profile.getData().getFull_name());
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            } else {
                TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }
      //============================================
      //============================================
        else if(flag.equals(DataEnum.call_forget_pwd.name())){
            ModelForgetPassword ModelForgetPassword = (ModelForgetPassword) o;
            com.game.of.riches.model.forgetpwd.Status status = ModelForgetPassword.getStatus();
            if(status.getStatus()){
                TastyToast.makeText(this, status.getMessage(), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
            }else{
                TastyToast.makeText(this, status.getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }

    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }
}
