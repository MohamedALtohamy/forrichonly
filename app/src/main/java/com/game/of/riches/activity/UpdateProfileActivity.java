package com.game.of.riches.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.game.of.riches.R;
import com.game.of.riches.interfaces.HandleRetrofitResp;
import com.game.of.riches.model.profile.Data;
import com.game.of.riches.model.profile.Profile;
import com.game.of.riches.retrofitconfig.HandelCalls;
import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.greysonparrelli.permiso.Permiso;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.sdsmdg.tastytoast.TastyToast;
import com.squareup.picasso.Picasso;
import com.van.fanyu.library.Compresser;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends BaseActivity implements HandleRetrofitResp,
    com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {

  //region fields
  @BindView(R.id.settings_image)
  CircleImageView settingsImage;
  @BindView(R.id.sp)
  Space sp;
  @BindView(R.id.switch_location)
  SwitchCompat switchLocation;
  @BindView(R.id.status)
  EditText status;
  @BindView(R.id.setting_update_btn)
  Button settingUpdateBtn;
  @BindView(R.id.email)
  TextView email;
  @BindView(R.id.tv_country)
  TextView tvCountry;
  @BindView(R.id.tv_birthdate)
  TextView tvBirthdate;
  @BindView(R.id.tv_male)
  TextView tvMale;
  @BindView(R.id.tvLevel)
  TextView tvLevel;

  @BindView(R.id.tv_female)
  TextView tvFemale;

  @BindView(R.id.rel_social)
  RelativeLayout relSocial;

  @BindView(R.id.profile_fullname)
  EditText profile_fullname;


  Toolbar mToolbar;

  //endregion
  //gender 0 not selected 1 male 2 female
  private int gender = 1;
  private Calendar calendar;
  private Uri ImageUri;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    ButterKnife.bind(this);
    calendar = Calendar.getInstance();
    Permiso.getInstance().setActivity(this);
    initToolBar();
    initValues();
    initGender();
   initAddMob();
    if (getIntent().hasExtra("fromregister")) {
      tvBirthdate.setVisibility(View.GONE);
      profile_fullname.setVisibility(View.GONE);
     // tvCountry.setVisibility(View.GONE);
    }

    // showCountry(this);

    //  DialogInfoApp.showDialogSearch(this);

    //avatar

    // HandelCalls.getInstance(UpdateProfileActivity.this).call(DataEnum.updateProfile.name(), meMap, true, this);

  }
   /* @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }*/

  private void initValues() {

    Data profile = (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(), Data.class);

    Picasso.with(this)
        .load(isUnkonwn(profile.getId(),profile.getAvatar()))
        .placeholder(R.drawable.default_avatar).into(settingsImage);
    email.setText(profile.getEmail());
    profile_fullname.setText(profile.getFull_name());
    tvLevel.setText(
        HelpMe.getInstance(this).initListLevels().get(Integer.parseInt(profile.getLevel())));
    setTextV(tvCountry, profile.getCountry_code());
    setTextV(tvBirthdate, profile.getDate_of_birth());
    gender = Integer.parseInt(profile.getGender());
    if(gender==0){
      gender=1;
    }
    if (!profile.getStatus_note().equals("")) {
      status.setText(profile.getStatus_note());
    }

  }
private void initAddMob() {
    AdView mAdView = findViewById(R.id.adView);

    HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_update_profile));
  }

  private void setTextV(TextView tv, String value) {
    if (!value.equals("")) {
      tv.setText(value);
    }
  }

  private String isUnkonwn(String userId,String image) {
    if (image.equals("")) {
      return getString(R.string.unknown);
    } else {
      return fullUrl(userId,image);

    }
  }


  private void initGender() {
    if (gender == 1) {
      tvMale.setBackground(getResources().getDrawable(R.drawable.active_gender));
      tvFemale.setBackground(getResources().getDrawable(R.drawable.gender));
    } else if (gender == 2) {
      tvMale.setBackground(getResources().getDrawable(R.drawable.gender));
      tvFemale.setBackground(getResources().getDrawable(R.drawable.active_gender));
    } else {
      tvMale.setBackground(getResources().getDrawable(R.drawable.gender));
      tvFemale.setBackground(getResources().getDrawable(R.drawable.gender));
    }
  }

  private void initToolBar() {
    mToolbar = (Toolbar) findViewById(R.id.update_profile_toolbar);
    setSupportActionBar(mToolbar);
    if (!getIntent().hasExtra("fromregister")) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    getSupportActionBar().setTitle(getString(R.string.update_profile));
  }

  @Override
  public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }

  @Override
  public void onBackPressed() {

    super.onBackPressed();
  }

  @OnClick({R.id.settings_image, R.id.setting_update_btn})
  public void onViewClickedImage(View view) {
    switch (view.getId()) {
      case R.id.settings_image:
        Permission();
        break;
      case R.id.setting_update_btn:
        if (allvalidate()) {
          updateProfile();
        }

        break;
    }
  }

  private boolean allvalidate() {
    if (gender == 0) {
      TastyToast.makeText(this, getString(R.string.err_msg_geneder), TastyToast.LENGTH_LONG,
          TastyToast.ERROR);
      return false;
    }
    if (tvCountry.getText().toString().isEmpty()) {
      TastyToast.makeText(this, getString(R.string.err_msg_country), TastyToast.LENGTH_LONG,
          TastyToast.ERROR);
      return false;
    }
    if (profile_fullname.getText().toString().isEmpty()) {
      TastyToast.makeText(this, getString(R.string.err_msg_full_name), TastyToast.LENGTH_LONG,
          TastyToast.ERROR);
      return false;
    }
    if (status.getText().toString().isEmpty()) {
      TastyToast.makeText(this, getString(R.string.status), TastyToast.LENGTH_LONG,
          TastyToast.ERROR);
     status.setError(getString(R.string.err_msg_required));
      return false;
    }
    else {
      status.setError(null);
      return true;
    }
  }

  private void updateProfile() {

    if (ImageUri == null) {
      HashMap<String, String> meMap = new HashMap<String, String>();
      meMap.put("country_code", tvCountry.getText().toString());
      //1 male 2 female
      meMap.put("gender", gender + "");
      // meMap.put("level","Red");
      meMap.put("date_of_birth", tvBirthdate.getText().toString());
      // meMap.put("long_point", "Red");
      //  meMap.put("lat_point", "Red");
      meMap.put("status_note", status.getText().toString());
      meMap.put("full_name", profile_fullname.getText().toString());
      // meMap.put("facebook_acc", "Red");
      // meMap.put("whatsapp_acc", "Red");
      // meMap.put("telegram_acc", "Red");
      //   meMap.put("instagram_acc", "Red");
      meMap.put("_method", "put");
      HandelCalls.getInstance(UpdateProfileActivity.this)
          .call(DataEnum.updateProfile.name(), meMap, true, this);
    } else {
      HashMap<String, RequestBody> meMap = new HashMap<String, RequestBody>();
      meMap.put("country_code", createPartFromString(tvCountry.getText().toString()));
      //1 male 2 female
      meMap.put("gender", createPartFromString(gender + ""));
      // meMap.put("level","Red");
      meMap.put("date_of_birth", createPartFromString(tvBirthdate.getText().toString()));
      // meMap.put("long_point", "Red");
      //  meMap.put("lat_point", "Red");
      meMap.put("status_note", createPartFromString(status.getText().toString()));
      meMap.put("full_name", createPartFromString(profile_fullname.getText().toString()));
      // meMap.put("facebook_acc", "Red");
      // meMap.put("whatsapp_acc", "Red");
      // meMap.put("telegram_acc", "Red");
      //   meMap.put("instagram_acc", "Red");
      meMap.put("_method", createPartFromString("put"));
      HandelCalls.getInstance(UpdateProfileActivity.this)
          .callWithImage(DataEnum.updateProfile.name(), meMap, prepareFilePart("avatar", ImageUri),
              true, this);
    }


  }

  @NonNull
  private RequestBody createPartFromString(String description) {
    return RequestBody.create(okhttp3.MultipartBody.FORM, description);
  }

  @NonNull
  private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
    File file = new File(fileUri.getPath());
    // create RequestBody instance from file
    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
    // MultipartBody.Part is used to send also the actual file name
    return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
  }

  @OnClick({R.id.tv_country, R.id.tv_birthdate, R.id.tv_male, R.id.tv_female, R.id.rel_social})
  public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.tv_country:
        showCountry(UpdateProfileActivity.this);
        break;
      case R.id.tv_birthdate:
        showCalendar();
        break;
      case R.id.tv_male:
        gender = 1;
        initGender();
        break;
      case R.id.tv_female:
        gender = 2;
        initGender();
        break;
      case R.id.rel_social:
        //  DialogInfoApp.showDialogSocial(this);
        break;
    }
  }

  public void showCountry(AppCompatActivity activity) {
    CountryPicker countryPicker =
        new CountryPicker.Builder().with(activity)
            .listener(new OnCountryPickerListener() {
              @Override
              public void onSelectCountry(Country country) {
                //DO something here
                Log.e("country", country.getName());
                tvCountry.setText(country.getName());
              }
            })
            .build();

    countryPicker.showDialog(activity.getSupportFragmentManager());
    //  Country country = countryPicker.getCountryFromSIM();
    //  Log.e("country",country.getName()+" "+country.getCode() );
  }

  private void showCalendar() {
    com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog
        .newInstance(
            UpdateProfileActivity.this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        );
    //  dpd.setThemeDark(true);

    dpd.setMaxDate(calendar);
    dpd.vibrate(true);
    dpd.dismissOnPause(true);
    // dpd.showYearPickerFirst(true);
    dpd.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
    dpd.setAccentColor(getResources().getColor(R.color.goldenColor));
    // dpd.setVersion(showVersion2.isChecked() ? DatePickerDialog.Version.VERSION_2 : DatePickerDialog.Version.VERSION_1);
    dpd.show(getFragmentManager(), "Datepickerdialog");
  }


  @Override
  public void onResume() {
    super.onResume();
    Permiso.getInstance().setActivity(this);

  }


  private void Permission() {
    Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
      @Override
      public void onPermissionResult(Permiso.ResultSet resultSet) {
        if (resultSet.isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            && resultSet.isPermissionGranted(Manifest.permission.CAMERA)) {
          //Log.e("yss", "agree");

          picImage();

        } else if (
            resultSet.isPermissionPermanentlyDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                && resultSet.isPermissionPermanentlyDenied(Manifest.permission.CAMERA)) {
          Toast.makeText(getApplicationContext(), getString(R.string.permstion_need),
              Toast.LENGTH_LONG).show();
        } else {
          Toast.makeText(getApplicationContext(), getString(R.string.permstion_need),
              Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onRationaleRequested(Permiso.IOnRationaleProvided callback,
          String... permissions) {
        Permiso.getInstance().showRationaleInDialog(getString(R.string.title_dialog_permtion),
            getString(R.string.permtion_dialog_reason), null, callback);
      }

    }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
  }

  private void picImage() {
    TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(
        UpdateProfileActivity.this)
        .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
          @Override
          public void onImageSelected(Uri uri) {
            Log.d("ted", "uri: " + uri);
            Log.d("ted", "uri.getPath(): " + uri.getPath());
            compressedFile(uri);
            ImageUri = uri;

          }
        })
        //.setPeekHeight(getResources().getDisplayMetrics().heightPixels/2)
        .setPeekHeight(1200)
        .setTitle(getString(R.string.choose_image))
        .create();

    bottomSheetDialogFragment.show(getSupportFragmentManager());
  }

  private void compressedFile(final Uri uri) {
    // imageUri=uri;
    try {
      Compresser compresser = new Compresser(50, uri.getPath());
      compresser.doCompress(new Compresser.CompleteListener() {
        @Override
        public void onSuccess(String newPath) {
          //Log.e("from", "comprees");
          settingsImage.setImageURI(Uri.parse(newPath));
          // imageUri = Uri.parse(newPath);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  String fullUrl(String useId,String img) {
    Log.e("fullurl",Constant.baseUrl+ "public/uploads/avatar/" + useId+"/"+img);
    return Constant.imageUrl+ useId+"/"+img;
  }

  @Override
  public void onResponseSuccess(String flag, Object o) {
    Profile profile = (Profile) o;
    if (profile.getStatus().getStatus()) {
      PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();
      // Data s= (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(),Data.class);
      // Log.e("my nmae",profile.getData().getFull_name());
      if (getIntent().hasExtra("fromregister")) {
        Intent i = new Intent(UpdateProfileActivity.this, HomeActivity.class);
        i.putExtra("fromregister", "true");
        startActivity(i);
        finish();
      }
      // startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
      TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG,
          TastyToast.SUCCESS);
    } else {
      TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG,
          TastyToast.ERROR);
    }
  }

  @Override
  public void onNoContent(String flag, int code) {

  }

  @Override
  public void onResponseSuccess(String flag, Object o, int position) {

  }

  @Override
  public void onBadRequest(String flag, Object o) {

  }

  @Override
  public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year,
      int monthOfYear, int dayOfMonth) {
    calendar.set(year, monthOfYear, dayOfMonth);

    java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd",
        Locale.ENGLISH);
    tvBirthdate.setText(simpleDateFormat.format(calendar.getTime()));
  }

  @Override
  protected void attachBaseContext(Context cnt) {
    super.attachBaseContext(LocaleHelper.onAttach(cnt));
    Log.e("onbase", "base0");
  }
}
