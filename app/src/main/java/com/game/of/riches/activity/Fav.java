package com.game.of.riches.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import com.game.of.riches.R;
import com.game.of.riches.adapter.AdapterFav;
import com.game.of.riches.model.users.Data;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.util.ArrayList;
import java.util.List;

public class Fav extends BaseActivity {

  private LinearLayout linearMap, LinearList;
  private RecyclerView.LayoutManager mLayoutManager;
  private SuperRecyclerView mRecycler;
  private AdapterFav mAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fav);
    initToolBar();
    initRec();
initAddMob();
  }

  private void initToolBar() {
    Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_fav);
    setSupportActionBar(mToolbar);
    mToolbar.setTitle(getString(R.string.title_activity_fav));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);    // getSupportActionBar().setTitle("Login");
   // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
  }
  private void initAddMob() {
    AdView mAdView = findViewById(R.id.adView);
    HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_fav));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem menuItem) {
    if (menuItem.getItemId() == android.R.id.home) {

      finish();
    }
    return super.onOptionsItemSelected(menuItem);
  }
  @Override
  public void onBackPressed() {
    super.onBackPressed();
    Log.e("back","finshed");
    finish();
  }

  private void initRec() {

    mRecycler = (SuperRecyclerView) findViewById(R.id.list);
    mRecycler.hideProgress();
    mLayoutManager = new GridLayoutManager(this, 1);
    mRecycler.setLayoutManager(mLayoutManager);
    List<Data> dataStored = PrefsUtil.with(this).get(Data.class, "fav");
    if (dataStored != null) {
      mAdapter = new AdapterFav(dataStored, Fav.this);
    } else {
      mAdapter = new AdapterFav(new ArrayList<Data>(), Fav.this);
    }

    mRecycler.setAdapter(mAdapter);
    //mRecycler.setupMoreListener(this, 1);

  }

}
