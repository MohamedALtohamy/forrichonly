package com.game.of.riches.retrofitconfig;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.game.of.riches.interfaces.HandleRetrofitResp;
import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.DialogActivity;
import com.game.of.riches.utlitites.LoadingDialog;
import com.sdsmdg.tastytoast.TastyToast;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by lenovo on 1/3/2018.
 */

public class HandelCalls {
    /**
     * Created by lenovo on 6/28/2017.
     */

    public static final String TAG = HandelCalls.class.getSimpleName();


    private static Context context;
    private static HandelCalls instance = null;
    private static RestRetrofit restRetrofit;
    private HandleRetrofitResp onRespnse;

    //private HandleNoContent onNoContent;

    /**
     * @param context create ana object if it's not already created (singleton)
     * @return reference to that class
     */
    public static HandelCalls getInstance(Context context) {

        HandelCalls.context = context;

        if (instance == null) {
            instance = new HandelCalls();
            restRetrofit = RestRetrofit.getInstance(context);
        }
        return instance;
    }

    /**
     * @param onRespnseSucess
     */
    public void setonRespnseSucess(HandleRetrofitResp onRespnseSucess) {
        this.onRespnse = onRespnseSucess;
    }


    public void call(final String flag, HashMap<String, String> meMap, Boolean ShowLoadingDialog, HandleRetrofitResp onRespnseSucess) {
        this.onRespnse = onRespnseSucess;
        if (flag.equals(DataEnum.LoginApiCall.name())) {
            callRetrofit(restRetrofit.getClientService().login(meMap), flag, ShowLoadingDialog);
        } else if (flag.equals(DataEnum.registerCall.name())) {
            callRetrofit(restRetrofit.getClientService().register(meMap), flag, ShowLoadingDialog);
        } else if (flag.equals(DataEnum.updateProfile.name())) {
            callRetrofit(restRetrofit.getClientService().updateProfil(meMap), flag, ShowLoadingDialog);
        } else if (flag.equals(DataEnum.callMyProfile.name())) {
            callRetrofit(restRetrofit.getClientService().getMYProfile(), flag, ShowLoadingDialog);
        } else if (flag.equals(DataEnum.callAllProfiles.name())) {
            callRetrofit(restRetrofit.getClientService().getProfiles(meMap), flag, ShowLoadingDialog);
        } else if (flag.equals(DataEnum.call_get_profile_next_page.name())) {
            String nextPage = meMap.get(DataEnum.call_get_profile_next_page.name());
            callRetrofit(restRetrofit.getClientService().getProfilesNextpage(nextPage), flag, ShowLoadingDialog);
        }
        else if (flag.equals(DataEnum.call_get_braintree_token.name())) {
            String link = Constant.braintree;
            callRetrofit(restRetrofit.getClientService().getBraintreeToken(link), flag, ShowLoadingDialog);
        }
        else if (flag.equals(DataEnum.call_get_braintree_check_out.name())) {
            String link = Constant.braintree;
            callRetrofit(restRetrofit.getClientService().BrainTreeCHeckOut(link,meMap), flag, ShowLoadingDialog);
        }
        else if (flag.equals(DataEnum.call_forget_pwd.name())) {
            callRetrofit(restRetrofit.getClientService().forgetpassword(meMap), flag, ShowLoadingDialog);
        }
    }


    public void callWithImage(final String flag, @PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part postImage, Boolean ShowLoadingDialog, HandleRetrofitResp onRespnseSucess) {
        this.onRespnse = onRespnseSucess;
        if (flag.equals(DataEnum.updateProfile.name())) {

            callRetrofit(restRetrofit.getClientService().updateProfileWithImage(partMap, postImage), flag, ShowLoadingDialog);
        }


    }


    public <T> void callRetrofit(Call<T> call, final String flag, final Boolean ShowDialog) {
        final LoadingDialog progressDialog = new LoadingDialog(context);
        if (ShowDialog) {
            progressDialog.show();
        }


        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Log.d("test", "onResponse() called with: call = [" + call + "], response = [" + response + "]" + "response returned");

                if (ShowDialog) {
                    progressDialog.dismiss();
                }


                Log.e(TAG, "onResponse: " + response.toString());

                if (response.code() == 200) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (onRespnse != null)
                            Log.d("testing", "onResponse() minma called with: call = [" + call + "], response = [" + response + "]");
                        onRespnse.onResponseSuccess(flag, response.body());

                    }
                    // TODO - 4 Add 400 to condition base on (Login Response)
                } else if (response.code() == 400 || response.code() == 401) {
                    Log.e("res1", "resp");
                    if (onRespnse != null) {
                        Log.e("res2", "resp");
                        try {
                            Log.e("res3", "resp");
                            // Log.e("resp",response.errorBody().string());
                            // onRespnse.onBadRequest(flag, response.errorBody().string());
                            // Log.e("resp",response.errorBody().string());
                            JSONObject o = new JSONObject(response.errorBody().string());
                            //{"status":{"status":0,"code":400,"message":"The selected email is invalid."}}

                          //  onRespnse.onBadRequest(flag, response.errorBody().string());
                            TastyToast.makeText(context, o.getString("message"), TastyToast.LENGTH_LONG, TastyToast.ERROR);
//{"status":{"status":0,"code":401,"message":"Warning this booking is overlapping with other bookings Dates."}}
                            //401
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {

                if (ShowDialog)
                    progressDialog.dismiss();
                if (t instanceof IOException) {
                    Log.e("nt","problem");
                    context.startActivity(new Intent(context, DialogActivity.class));
                }
                else{
                    TastyToast.makeText(context, t.getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
                }

            }
        });

    }
}
