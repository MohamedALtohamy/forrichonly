package com.game.of.riches.utlitites;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;


import com.game.of.riches.R;
import com.race604.drawable.wave.WaveDrawable;

/**
 * Created by ksi on 27-Mar-18.
 */

public class LoadingDialog extends AppCompatDialog {

    public LoadingDialog(Context context) {
        super(context);
        setup();
    }

    @SuppressWarnings("ConstantConditions")
    private void setup(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setGravity(Gravity.CENTER);
        setContentView(R.layout.layout_loading_indicator);
        setCancelable(false);

        AppCompatImageView ivLoading = findViewById(R.id.ivLoading);
        WaveDrawable drawable = new WaveDrawable(getContext(), R.drawable.ic_launcher_pg);
        ivLoading.setImageDrawable(drawable);
        drawable.setIndeterminate(true);
        drawable.setWaveAmplitude(30);
        drawable.setWaveLength(70);
        drawable.setWaveSpeed(65);
    }
}
