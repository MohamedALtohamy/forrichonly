package com.game.of.riches.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.crashlytics.android.Crashlytics;
import com.game.of.riches.BuildConfig;
import com.game.of.riches.R;
import com.game.of.riches.dialog.DialogInfoApp;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.LoadingDialog;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import io.fabric.sdk.android.Fabric;

public class StartActivity extends BaseActivity{


    private Button mRegBtn;
    private Button mLoginBtn;
    private    FirebaseRemoteConfig mFirebaseRemoteConfig;
 private    LoadingDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_start);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
      initAddMob();


        /*
        if(PrefsUtil.with(this).get(DataEnum.isFreeUnabled.name(),"").equals("")){
            progressDialog = new LoadingDialog(this);
            progressDialog.show();
            initFireBASErEMOTEcONFIG();
      }else{
            initFireBASErEMOTEcONFIG();
        }
*/

        //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        findViewById(R.id.tvDescreption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //DialogInfoApp.showDialog(StartActivity.this, "");
                DialogInfoApp.showDialogInfo(StartActivity.this,getString(R.string.info_app));

            }
        });
        mRegBtn = (Button) findViewById(R.id.start_reg_btn);
        mLoginBtn = (Button) findViewById(R.id.start_login_btn);

        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent reg_intent = new Intent(StartActivity.this, RegisterActivity.class);
                startActivity(reg_intent);

            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent login_intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(login_intent);

            }
        });

    }

  private void initAddMob() {
    AdView mAdView = findViewById(R.id.adView);

    HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_star_act));
  }
  /* MobileAds.initialize(this, getString(R.string.app_mobads));
    AdView adView = new AdView(this);
    adView.setAdSize(AdSize.BANNER);
    adView.setAdUnitId(getString(R.string.mobads_star_act));


    AdRequest adRequest = new AdRequest.Builder().build();
    mAdView.loadAd(adRequest);*/

  @Override
  protected void attachBaseContext(Context cnt) {
    super.attachBaseContext(LocaleHelper.onAttach(cnt));
    Log.e("onbase","base0");
  }

    private void initFireBASErEMOTEcONFIG() {
         mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        mFirebaseRemoteConfig.fetch(1)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            mFirebaseRemoteConfig.activateFetched();
                            boolean isEnableFree = mFirebaseRemoteConfig.getBoolean("free");
                            PrefsUtil.with(StartActivity.this).add(DataEnum.isFreeUnabled.name(),isEnableFree+"").apply();
                            Log.e("issucess",isEnableFree+"");
                        }else{
                            boolean isEnableFree = mFirebaseRemoteConfig.getBoolean("free");
                            PrefsUtil.with(StartActivity.this).add(DataEnum.isFreeUnabled.name(),isEnableFree+"").apply();
                            Log.e("issucessnoo",isEnableFree+"");
                        }
                        if(progressDialog!=null){
                         if(progressDialog.isShowing()){
                             progressDialog.dismiss();
                         }
                        }


                    }
                });
    }

}
