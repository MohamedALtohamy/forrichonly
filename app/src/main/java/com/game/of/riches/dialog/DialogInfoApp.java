package com.game.of.riches.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.game.of.riches.R;
import com.game.of.riches.activity.HomeActivity;

import com.game.of.riches.activity.LoginActivity;
import com.game.of.riches.model.profile.Data;

import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.DialogActivity;
import com.game.of.riches.utlitites.PrefsUtil;
import com.hbb20.CountryCodePicker;

import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;

import java.util.HashMap;


import butterknife.OnClick;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

/**
 * Created by mo on 7/14/2018.
 */

public class DialogInfoApp {


    private static DialogInfoApp instance = null;
    private static Context context;
    private  int gender=0;

    public static DialogInfoApp getInstance(Context context) {

        DialogInfoApp.context = context;

        if (instance == null) {
            instance = new DialogInfoApp();

        }
        return instance;
    }

    public static void showDialog(Activity activity, String msg) {
        msg = activity.getResources().getString(R.string.info_app);
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_info);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    public static void initDialogForgetPwd(final LoginActivity activity) {
        //==========================================//
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        final EditText edittext = new EditText(activity);
        alert.setMessage(activity.getString(R.string.forget_pwd_enter_your_email));
        //  alert.setTitle("Enter Your Title");

        alert.setView(edittext);

        alert.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String email = edittext.getText().toString();
                //==================================================//
                if (!email.isEmpty()) {
                  activity.callResetPwd(email);


                }
                //===================================================//

            }
        });

        alert.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }

    public void showDialogSocial(final HomeActivity activity) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        View view = View.inflate(context, R.layout.dialog_social_list, null);
       // ButterKnife.bind(context, view);
        dialog.setContentView(view);
       dialog.getWindow() .setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        TextView dialog_cancel = (TextView) dialog.findViewById(R.id.dialog_cancel);
        TextView btnOk = (TextView) dialog.findViewById(R.id.btnOk);
        LinearLayout linear_fb = (LinearLayout) dialog.findViewById(R.id.linear_fb);
        LinearLayout linear_whats = (LinearLayout) dialog.findViewById(R.id.linear_whats);
        LinearLayout linear_instgram = (LinearLayout) dialog.findViewById(R.id.linear_instgram);
        LinearLayout linear_telegram = (LinearLayout) dialog.findViewById(R.id.linear_telegram);
        //
        final EditText edt_fb = (EditText) dialog.findViewById(R.id.edt_fb);
        final EditText edt_telegram = (EditText) dialog.findViewById(R.id.edt_telegram);
        final EditText edt_instgram = (EditText) dialog.findViewById(R.id.edt_instgram);
      //  final PhoneEditText phoneField = (PhoneEditText) dialog.findViewById(R.id.edt_phone);
      final   CountryCodePicker ccp=  dialog.findViewById(R.id.ccp);
        final EditText edtWhatsNumber = (EditText) dialog.findViewById(R.id.edtWhatsNumber);
        ccp.registerCarrierNumberEditText(edtWhatsNumber);
      //  final EditText edt_country_code = (EditText) dialog.findViewById(R.id.edt_country_code);
        Data profile = (Data) PrefsUtil.with(activity).get(DataEnum.shProfile.name(), Data.class);
        edt_fb.setText(profile.getFacebook_acc());
        edt_instgram.setText(profile.getInstagram_acc());
        edt_telegram.setText(profile.getTelegram_acc());
        //whats




       // countryPicker.showDialog(activity.getSupportFragmentManager());
       // Country country = countryPicker.getCountryFromSIM();
        String strWhats=profile.getWhatsapp_acc();
        if(!profile.getWhatsapp_acc().equals("")){
           // phoneField.setPhoneNumber(strWhats);
           // String code=country.getDialCode().replace("+","");
            ccp.setFullNumber(strWhats);
           // edtWhatsNumber.setText(strWhats);
          // edt_whats_number.setPhoneNumber(strWhats);
        //ccp.setDefaultCountryUsingNameCode(country.getCode());
        //  ccp.get
        }

       // country.getDialCode()
        //


        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> meMap=new HashMap<String, String>();
               if(!edt_fb.getText().toString().isEmpty()){
                   if(edt_fb.getText().toString().contains("/")){
                       int lastindex=edt_fb.getText().toString().lastIndexOf("/");
                       String fb=edt_fb.getText().toString().substring(lastindex+1);
                       meMap.put("facebook_acc",fb);
                   }else{
                       meMap.put("facebook_acc",edt_fb.getText().toString());
                   }

               }
               else{
                 meMap.put("facebook_acc","");
               }
                if(!edt_telegram.getText().toString().isEmpty()){
                    meMap.put("telegram_acc",edt_telegram.getText().toString());
                }
                else{
                  meMap.put("telegram_acc","");
                }
                if(!edt_instgram.getText().toString().isEmpty()){
                    meMap.put("instagram_acc",edt_instgram.getText().toString());
                }else{
                  meMap.put("instagram_acc","");
                }

                if(edtWhatsNumber.getText().toString().length()>0){
                   if(ccp.isValidFullNumber()){
                       meMap.put("whatsapp_acc",ccp.getFullNumber());
                       activity.updateSocilaAccount(meMap);
                       dialog.dismiss();
                   }else{
                       edtWhatsNumber.setError(activity.getString(R.string.invalid_phone));
                   }

                }
                else{
                  meMap.put("whatsapp_acc","");
                    dialog.dismiss();
                    activity.updateSocilaAccount(meMap);

                }







            }
        });
        linear_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(context.getString(R.string.fb_help));
            }
        });
        linear_whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        linear_instgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              openLink(Constant.instaHelp);
            }
        });
        linear_telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(context.getString(R.string.telegram_help));
            }
        });
        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showDialogSearch(final HomeActivity activity) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        View view = View.inflate(activity, R.layout.dialog_search_list, null);
        dialog.setContentView(view);
        dialog.getWindow() .setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView dialog_cancel = (TextView) dialog.findViewById(R.id.dialog_cancel);
        TextView btnOk = (TextView) dialog.findViewById(R.id.btnOk);
        final TextView tv_country = (TextView) dialog.findViewById(R.id.tv_country);
        final TextView tv_defualt = (TextView) dialog.findViewById(R.id.tv_defualt);

        final TextView tvMale = (TextView) dialog.findViewById(R.id.tv_male);
        final TextView tvFemale = (TextView) dialog.findViewById(R.id.tv_female);
      //  final int gender = 0;
        if (gender == 1) {
            tvMale.setBackground(activity.getResources().getDrawable(R.drawable.active_gender));
            tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
        } else if (gender == 2) {
            tvMale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
            tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.active_gender));
        } else {
            tvMale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
            tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
        }

        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountryPicker countryPicker =
                        new CountryPicker.Builder().with(activity)
                                .listener(new OnCountryPickerListener() {
                                    @Override
                                    public void onSelectCountry(Country country) {
                                        //DO something here
                                        Log.e("country", country.getName());
                                        tv_country.setText(country.getName());

                                    }
                                })
                                .build();


                countryPicker.showDialog(activity.getSupportFragmentManager());
            }
        });

        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tv_defualt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender=0;
                tv_country.setText("");
                tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
                tvMale.setBackground(activity.getResources().getDrawable(R.drawable.gender));

            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> meMap = new HashMap<String, String>();
                if(gender!=0){
                    meMap.put("gender",gender+"");
                }
                if(!tv_country.getText().toString().isEmpty()){
                    meMap.put("country_code",tv_country.getText().toString());
                }
                activity.loadData(meMap);
               dialog.dismiss();
            }
        });
        tvMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //  gender =1;
                setGender(1);
                tvMale.setBackground(activity.getResources().getDrawable(R.drawable.active_gender));
                tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
            }
        });
        tvFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  gender =2;
                setGender(2);
                tvMale.setBackground(activity.getResources().getDrawable(R.drawable.gender));
                tvFemale.setBackground(activity.getResources().getDrawable(R.drawable.active_gender));
            }
        });
        dialog.show();

    }
    private  void setGender (int gender){
        this.gender=gender;

    }
    public static  void showDialogConfirmExit(final Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null)
            builder.setTitle(Html.fromHtml("<font color='red'>" + title + "</font>"));

        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // int pid = android.os.Process.myPid();
                // android.os.Process.killProcess(pid);

                try {
                   activity. finishAffinity();
                    if (Build.VERSION.SDK_INT >= 21) {
                       activity. finishAndRemoveTask();
                    }

                    int pid = android.os.Process.myPid();
                    android.os.Process.killProcess(pid);
                } catch (Exception e) {

                }
            }

        });
        builder.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        builder.show();
    }
    public static void showDialogInfo(Activity activity, String msg) {

        final PrettyDialog dialog = new PrettyDialog(activity);
        dialog.setTitle(activity.getString(R.string.app_name))
                .setTitleColor(R.color.goldenColor)
                .setMessage(msg)
                .setIcon(R.drawable.ic_info_outline_black_24dp)
                .addButton(
                        activity.getString(R.string.ok),                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.pdlg_color_green,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                dialog.dismiss();
                            }
                        }
                )
                .show();

    }
  public static void showDialogInfoUpdateLevel(final HomeActivity activity, String msg,final String level) {

    final PrettyDialog dialog = new PrettyDialog(activity);
    dialog.setTitle(activity.getString(R.string.app_name))
        .setTitleColor(R.color.goldenColor)
        .setMessage(msg)
        .setIcon(R.drawable.ic_info_outline_black_24dp)
        .addButton(
            activity.getString(R.string.ok),                    // button text
            R.color.pdlg_color_white,        // button text color
            R.color.pdlg_color_green,        // button background color
            new PrettyDialogCallback() {        // button OnClick listener
              @Override
              public void onClick() {
                dialog.dismiss();
                activity.isServiceInAppAvailable(level);

              }
            }
        )
        .show();

  }
  public static void showDialogInfoUpdateLevelMsg(final AppCompatActivity activity, String msg,final String level) {

    final PrettyDialog dialog = new PrettyDialog(activity);
    dialog.setTitle(activity.getString(R.string.app_name))
        .setTitleColor(R.color.goldenColor)
        .setMessage(msg)
        .setIcon(R.drawable.ic_info_outline_black_24dp)
        .addButton(
            activity.getString(R.string.ok),                    // button text
            R.color.pdlg_color_white,        // button text color
            R.color.pdlg_color_green,        // button background color
            new PrettyDialogCallback() {        // button OnClick listener
              @Override
              public void onClick() {
                dialog.dismiss();
               // activity.isServiceInAppAvailable(level);

              }
            }
        )
        .show();

  }
    public static void showDialogError(Activity activity, String msg) {

        final PrettyDialog dialog = new PrettyDialog(activity);
        dialog.setTitle(activity.getString(R.string.app_name))
                .setTitleColor(R.color.goldenColor)
                .setMessage(msg)
                .setIcon(R.drawable.ic_info_outline_black_24dp)
                .addButton(
                        activity.getString(R.string.ok),                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.pdlg_color_green,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                dialog.dismiss();
                            }
                        }
                )
                .show();

    }

    public static void showCountry(final AppCompatActivity activity) {
        CountryPicker countryPicker =
                new CountryPicker.Builder().with(activity)
                        .listener(new OnCountryPickerListener() {
                            @Override
                            public void onSelectCountry(Country country) {
                                //DO something here
                                Log.e("country", country.getName());

                            }
                        })
                        .build();


        countryPicker.showDialog(activity.getSupportFragmentManager());
    }

    private void openLink(String link) {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        context.startActivity(i);
    }

    @OnClick({R.id.linear_fb, R.id.linear_whats, R.id.linear_telegram, R.id.linear_instgram, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_fb:
                openLink(context.getString(R.string.fb_help));
                Log.e("fb", "fb");
                break;
            case R.id.linear_whats:
                break;
            case R.id.linear_telegram:
                openLink(context.getString(R.string.telegram_help));
                break;
            case R.id.linear_instgram:
                break;
            case R.id.btnOk:
                break;
        }
    }
    public static void noConnection(final DialogActivity activty) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(activty);
            String title = activty.getString(R.string.dialog_no_internt_title);
            String msg = activty.getString(R.string.dialog_no_internt_msg);
            if (title != null)
                builder.setTitle(Html.fromHtml("<font color='red'>" + title + "</font>"));

            builder.setMessage(msg);
            builder.setCancelable(false);
            builder.setPositiveButton(activty.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    activty.finish();
                }
            });


            builder.show();
        } catch (Exception e) {


        }
    }
}
