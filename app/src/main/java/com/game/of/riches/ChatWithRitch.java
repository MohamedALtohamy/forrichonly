package com.game.of.riches;

import static com.facebook.AccessTokenManager.TAG;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.util.Base64;
import android.util.Log;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created
 */

public class ChatWithRitch extends Application {


  @Override
  public void onCreate() {
    super.onCreate();
   // printHashKey(getApplicationContext());
       /* Fonty
                .context(this)
                .normalTypeface("DroidKufi-Regular.ttf")
                .italicTypeface("DroidKufi-Regular.ttf")
                .boldTypeface("DroidKufi-Regular.ttf")
                .build();*/
    //  initFont();

    /* Picasso */
    initLang();
        /*
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
*/

    initFont();


  }

  /*
  @Override
  protected void attachBaseContext(Context base) {
      String lang = PrefsUtil.with(getApplicationContext()).get(DataEnum.shLang.name(), "");
      if (lang.equals("")) {
          String c = Locale.getDefault().getDisplayLanguage();
          if (c.equals("العربية")) {
              lang = "ar";
          } else {
              lang = "en";
          }
      }
      super.attachBaseContext(LocaleHelper.onAttach(base, lang));
  }
*/
  private void initFont() {
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
        .setDefaultFontPath("fonts/DroidKufi-Regular.ttf")
        .setFontAttrId(R.attr.fontPath)
        .build()
    );
  }

  private void initLang() {

    String lang = PrefsUtil.with(getApplicationContext()).get(DataEnum.shLang.name(), "");
    if (lang.equals("")) {
      String c = Locale.getDefault().getDisplayLanguage();
      if (c.equals("العربية")) {
        lang = "ar";
      } else {
        lang = "en";
      }
    }
    PrefsUtil.with(getApplicationContext()).add(DataEnum.shLang.name(), lang).apply();
    Locale locale = new Locale(lang);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
        getBaseContext().getResources().getDisplayMetrics());
    LocaleHelper.setLocale(getApplicationContext(), lang);
    Log.e("langissss", lang);
  }
  public  void printHashKey(Context pContext) {
    try {
      PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
      for (android.content.pm.Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(Byte.parseByte(signature.toString()));
        String hashKey = new String(Base64.encode(md.digest(), 0));
        Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
      }
    } catch (NoSuchAlgorithmException e) {
      Log.e(TAG, "printHashKey()", e);
    } catch (Exception e) {
      Log.e(TAG, "printHashKey()", e);
    }
  }

}
