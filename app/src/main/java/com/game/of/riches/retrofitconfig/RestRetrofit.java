package com.game.of.riches.retrofitconfig;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.PrefsUtil;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import okhttp3.logging.HttpLoggingInterceptor;

// start comment
//import okhttp3.logging.HttpLoggingInterceptor;
// end comment


public class RestRetrofit {

    private static final String TAG = RestRetrofit.class.getSimpleName();

    private static RestRetrofit instance;
    private static ApiCall apiService;
    //public final String BASE_URL = "http://192.168.1.222/";
    private static Context mcontext;
    // public String apiValue = "9c4a06e4dddceb70722de4f3fda4f2c7";
    public String apiKey = "apiKey";
    public String Authorization ="token";
    private String deviceKey = "device";
   // private String deviceValue = "9584215687459532154865";
   private String deviceValue = "";


    private RestRetrofit() {


        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(6, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES);

/*
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);
*/
        //  httpClient.addInterceptor(interceptor).build();


        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request();
                Request newRequest;
                String apiValue = Constant.apiValue;
                String token = PrefsUtil.with(mcontext).get(DataEnum.AuthToken.name(), "");

                Log.d(TAG, "intercept() called with: token = [" + token + "]");

                deviceValue = PrefsUtil.with(mcontext).get(DataEnum.shFirebaseToken.name(), "utiututtt7t7g7tyyyrr");
                Log.e(TAG, "intercept:firebasetoken " + deviceValue);
                if (!token.isEmpty() && !deviceValue.isEmpty()) {
                    Log.d(TAG, "intercept() first called with: chain  = [" + chain + "]" + "\napiValue"+apiValue+"\nauthorization"+token+"\nfirebase "+deviceValue+"\n"+getVestionCode(mcontext));
                    newRequest = request.newBuilder()
                            .header(apiKey, apiValue)
                            .header(Authorization, token)
                            .header(deviceKey, deviceValue)
                            .header("version", getVestionCode(mcontext))
                            .method(request.method(), request.body())
                            .build();
                    return chain.proceed(newRequest);
                } else if (!token.isEmpty()) {
                    Log.d(TAG, "intercept() second called with: chain = [" + chain + "]" +apiValue+token+deviceValue+getVestionCode(mcontext));

                    newRequest = request.newBuilder()
                            .header(apiKey, apiValue)
                            .header(Authorization, token)
                            // todo remove this line when youssef make it possible to login wihout the device
                            .header(deviceKey, deviceValue)

                            .header("version", getVestionCode(mcontext))
                            .method(request.method(), request.body())
                            .build();
                    return chain.proceed(newRequest);
                } else if (!deviceValue.isEmpty()){
                    Log.d(TAG, "intercept() third called with: chain = [" + chain + "]" +apiValue+token+deviceValue+getVestionCode(mcontext));

                    newRequest = request.newBuilder()
                            .header(apiKey, apiValue)
                            .header(deviceKey, deviceValue)
                            .header("version", getVestionCode(mcontext))
                            .method(request.method(), request.body())
                            .build();
                    return chain.proceed(newRequest);
                } else {
                    Log.d(TAG, "intercept() fifth called with: chain = [" + chain + "]" +apiValue+token+deviceValue+getVestionCode(mcontext));

                    newRequest = request.newBuilder()
                            .header(apiKey, apiValue)
                            // todo remove this line when youssef make it possible to login wihout the device
                            .header(deviceKey, deviceValue)
                            .header("version", getVestionCode(mcontext))
                            .method(request.method(), request.body())
                            .build();
                    return chain.proceed(newRequest);
                }

            }
        });

        OkHttpClient httpClient = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        apiService = retrofit.create(ApiCall.class);
    }

    public static RestRetrofit getInstance(Context context) {
        mcontext = context;
        if (instance == null) {
            instance = new RestRetrofit();
        }
        return instance;
    }

    public static String getVestionCode(Context c) {
        /*
        p40sdmkkmgjb1ggyadqz
        e4bbe5b7a4c1eb55652965aee885dd59bd2ee7f4
         */
        String v = "";
        try {

            v += c.getPackageManager()
                    .getPackageInfo(c.getPackageName(), 0).versionName;
            PrefsUtil.with(c).add(DataEnum.shversionName.name(), v);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        // Log.e("log",v);
        return v;

    }

    public ApiCall getClientService() {

        return apiService;
    }
}