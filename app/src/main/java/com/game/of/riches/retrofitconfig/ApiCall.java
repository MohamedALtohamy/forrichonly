package com.game.of.riches.retrofitconfig;


import android.support.annotation.Keep;
import com.game.of.riches.model.forgetpwd.ModelForgetPassword;
import com.game.of.riches.model.profile.Profile;
import com.game.of.riches.model.users.ModelUsers;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
@Keep
public interface ApiCall {

    @FormUrlEncoded
    @POST("login")
    Call<Profile> login(@FieldMap HashMap<String, String> requestBody);

    @FormUrlEncoded
    @POST("register")
    Call<Profile> register(@FieldMap HashMap<String, String> requestBody);
    @FormUrlEncoded
    @POST("profile/update")
    Call<Profile> updateProfil(@FieldMap HashMap<String, String> requestBody);


    @Multipart
    @POST("profile/update")
    Call<Profile> updateProfileWithImage(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part postImage
    );

    @GET("profiles")
    Call<ModelUsers> getProfiles(@QueryMap HashMap<String, String>requestBody);
    @GET
    Call<ModelUsers> getProfilesNextpage(@Url String fileUrl);
    @GET("profile")
    Call<Profile> getMYProfile();

    @GET
    Call<String> getBraintreeToken(@Url String Url);
    @FormUrlEncoded
    @POST
    Call<String> BrainTreeCHeckOut(@Url String Url,@FieldMap HashMap<String, String> requestBody);
    @FormUrlEncoded
    @POST("forget-password")
    Call<ModelForgetPassword> forgetpassword(@FieldMap HashMap<String, String> requestBody);


}