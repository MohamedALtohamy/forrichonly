package com.game.of.riches.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.game.of.riches.BuildConfig;
import com.game.of.riches.R;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //  setContentView(R.layout.activity_splash_screen);
       // HelpMe.getInstance(this).initLang();
        initFireBASErEMOTEcONFIG();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String token = PrefsUtil.with(SplashScreen.this).get(DataEnum.AuthToken.name(), "");
                if(token.equals("")){
                    startActivity(new Intent(SplashScreen.this,StartActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(SplashScreen.this,HomeActivity.class));
                    finish();
                }

            }
        }, 3000);

    }

    private void initFireBASErEMOTEcONFIG() {
        final FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        mFirebaseRemoteConfig.fetch(1)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            mFirebaseRemoteConfig.activateFetched();
                            boolean isEnableFree = mFirebaseRemoteConfig.getBoolean("free");
                            boolean isEnablesearch = mFirebaseRemoteConfig.getBoolean("enable_search");
                            PrefsUtil.with(SplashScreen.this).add(DataEnum.isFreeSearch.name(), isEnablesearch + "").apply();
                            PrefsUtil.with(SplashScreen.this).add(DataEnum.isFreeUnabled.name(), isEnableFree + "").apply();
                            Log.e("issucess", isEnableFree + "");
                        } else {
                            boolean isEnableFree = mFirebaseRemoteConfig.getBoolean("free");
                            PrefsUtil.with(SplashScreen.this).add(DataEnum.isFreeUnabled.name(), isEnableFree + "").apply();
                            Log.e("issucessnoo", isEnableFree + "");
                        }



                    }



    });
}
}
