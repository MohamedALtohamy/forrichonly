package com.game.of.riches.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.ButterKnife;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.game.of.riches.R;
import com.game.of.riches.adapter.AdapterUsers;
import com.game.of.riches.adapter.SimpleListAdapter;
import com.game.of.riches.dialog.DialogInfoApp;
import com.game.of.riches.interfaces.HandleRetrofitResp;
import com.game.of.riches.model.profile.Profile;
import com.game.of.riches.model.users.Data;
import com.game.of.riches.model.users.ModelUsers;
import com.game.of.riches.retrofitconfig.HandelCalls;
import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sdsmdg.tastytoast.TastyToast;
import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.IStatusListener;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends BaseActivity implements OnMapReadyCallback, OnMoreListener,
    HandleRetrofitResp,
    PurchasesUpdatedListener {

  private GoogleMap mMap;

  private LinearLayout linearMap, LinearList;
  private RecyclerView.LayoutManager mLayoutManager;
  private SuperRecyclerView mRecycler;
  private AdapterUsers mAdapter;
  private String nextPage = "";
  private Toolbar mToolbar;
  private SearchableSpinner mSearchableSpinner;
  private SimpleListAdapter mSimpleArrayListAdapter;
  public ArrayList<String> mStrings = new ArrayList<>();
  public ArrayList<String> mStringsRmeainLevels = new ArrayList<>();
  private static final int BRAINTREE_REQUEST_CODE = 4949;
  private int level = 0;
  private String transactionId = "";
  private SwipeRefreshLayout mSwipeRefreshLayout;
  private BillingClient mBillingClient;
  private boolean isFirstShot = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    ButterKnife.bind(this);
    initToolBar();
    initView();
    initMap();
    initClick();
    initSpinierLib();
    initRec();
    initAddMob();
    HandelCalls.getInstance(this)
        .call(DataEnum.callAllProfiles.name(), new HashMap<String, String>(), true, this);
    if (getIntent().hasExtra("fromregister")) {
      DialogInfoApp.getInstance(this).showDialogSocial(this);
    }

  }

  @Override
  protected void attachBaseContext(Context cnt) {
    super.attachBaseContext(LocaleHelper.onAttach(cnt));
    Log.e("onbase", "base0");
  }

  private void initSpinierLib() {
    mStringsRmeainLevels = new ArrayList<>();
    com.game.of.riches.model.profile.Data profile = (com.game.of.riches.model.profile.Data) PrefsUtil
        .with(this).get(DataEnum.shProfile.name(), com.game.of.riches.model.profile.Data.class);

    mStrings = HelpMe.getInstance(HomeActivity.this).initListLevels();
    String cuerrentLevel = profile.getLevel();
    if (!cuerrentLevel.equals("0")) {
      int level = Integer.parseInt(cuerrentLevel);
      for (int i = 0; i < level; i++) {
        mStringsRmeainLevels.add(mStrings.get(i));
      }
      Collections.reverse(mStringsRmeainLevels);
      mSimpleArrayListAdapter = new SimpleListAdapter(this, mStringsRmeainLevels);
      mSearchableSpinner = (SearchableSpinner) findViewById(R.id.SearchableSpinner);
      mSearchableSpinner.setAdapter(mSimpleArrayListAdapter);
      mSearchableSpinner.setOnItemSelectedListener(mOnItemSelectedListener);

      mSearchableSpinner.setSelectedItem(1);
      mSearchableSpinner.setStatusListener(new IStatusListener() {
        @Override
        public void spinnerIsOpening() {

        }

        @Override
        public void spinnerIsClosing() {

        }
      });
    } else {
      findViewById(R.id.linearUpdateLevel).setVisibility(View.GONE);
    }

  }

  private OnItemSelectedListener mOnItemSelectedListener = new OnItemSelectedListener() {
    @Override
    public void onItemSelected(View view, int position, long id) {
      if (position > 0) {
        position = position - 1;
        String levelName = mStringsRmeainLevels.get(position);
        ArrayList<String> arrString = HelpMe.getInstance(HomeActivity.this)
            .initListLevels();
        position = arrString.indexOf(levelName);
        // position = position - 1;
        level = position;
        if (position < Constant.maxAvailableLevel) {

          level = -1;
          String levelStr = HelpMe.getInstance(HomeActivity.this).mStrings.get(position) + " ";
          DialogInfoApp.showDialogInfo(HomeActivity.this,
              levelStr + getString(R.string.dialog_no_available_level));


        }
      }

      //   position=position-1;
      // level=position+"";
      /* Toast.makeText(RegisterActivity.this, "Item on position " + position + " : " + mSimpleArrayListAdapter.getItem(position) + " Selected", Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onNothingSelected() {
      Toast.makeText(HomeActivity.this, R.string.not_select_level, Toast.LENGTH_SHORT).show();
    }
  };
   /* @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }*/

  @Override
  public void onBackPressed() {
    //  super.onBackPressed();
    Log.e("back", "tt");
    DialogInfoApp.showDialogConfirmExit(HomeActivity.this, getString(R.string.confirm_back),
        getString(R.string.confirm_exit_app));
  }

  @Override
  protected void onResume() {
    super.onResume();
    mToolbar.setTitle(getString(R.string.app_name));
  }

  private void initRec() {

    mRecycler = (SuperRecyclerView) findViewById(R.id.list);
    mRecycler.hideProgress();
    mLayoutManager = new GridLayoutManager(this, 1);
    mRecycler.setLayoutManager(mLayoutManager);
    mRecycler.setupMoreListener(this, 1);

  }

  private void initMap() {
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
  }

  private void initToolBar() {
    mToolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(mToolbar);
    mToolbar.setTitle(getString(R.string.app_name));
    // getSupportActionBar().setTitle("Login");
  }

  private void initView() {
    linearMap = findViewById(R.id.linearMap);
    mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(
        R.id.activity_main_swipe_refresh_layout);
    // mSwipeRefreshLayout.setRefreshing(true);
    mSwipeRefreshLayout.setOnRefreshListener(
        new SwipeRefreshLayout.OnRefreshListener() {
          @Override
          public void onRefresh() {

            // This method performs the actual data-refresh operation.
            // The method calls setRefreshing(false) when it's finished.
            //initRec();
            //  mRecycler.setOnMoreListener(null);
            //  mRecycler.setupMoreListener(HomeActivity.this, 1);
            //  loadData(new HashMap<String, String>());
            //recreate();
            startActivity(new Intent(HomeActivity.this, HomeActivity.class));
            finish();
          }
        }
    );
    findViewById(R.id.update_level).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.e("update", "update");
        if (level == -1) {
          TastyToast.makeText(HomeActivity.this, getString(R.string.validate_choose_level),
              TastyToast.LENGTH_LONG, TastyToast.ERROR);
        } else {
          // HandelCalls.getInstance(HomeActivity.this).call(DataEnum.call_get_braintree_token.name(), null, true, HomeActivity.this);
          isServiceInAppAvailable(level + "");
        }


      }
    });
    // LinearList = findViewById(R.id.LinearList);
  }


  public void isServiceInAppAvailable(final String level) {
    mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
    mBillingClient.startConnection(new BillingClientStateListener() {
      @Override
      public void onBillingSetupFinished(@BillingResponse int billingResponseCode) {
        if (billingResponseCode == BillingResponse.OK) {
          BillingFlowParams flowParams = BillingFlowParams.newBuilder()
              .setSku(level + "")
              .setType(SkuType.INAPP) // SkuType.SUB for subscription
              .build();
          int responseCode = mBillingClient.launchBillingFlow(HomeActivity.this, flowParams);
          Log.e("code", responseCode + "");
        }
      }

      @Override
      public void onBillingServiceDisconnected() {
        // Try to restart the connection on the next request to
        // Google Play by calling the startConnection() method.
      }
    });
  }

  @Override
  public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
    if (responseCode == BillingResponse.OK
        && purchases != null) {
      for (Purchase purchase : purchases) {
        handlePurchase(purchase);
      }
    } else if (responseCode == BillingResponse.USER_CANCELED) {
      //  DialogInfoApp.showDialogInfo(this, "cancelled");
      // Handle an error caused by a user cancelling the purchase flow.
    } else {
      // DialogInfoApp.showDialogInfo(this, "Error");
      // Handle any other error codes.
    }

  }

  private void handlePurchase(Purchase purchase) {
    transactionId = purchase.getOriginalJson();
    updateLevel();

  }

  public void loadData(HashMap<String, String> meMap) {
    mSwipeRefreshLayout.setRefreshing(false);
    HandelCalls.getInstance(this).call(DataEnum.callAllProfiles.name(), meMap, true, this);
  }

  public void updateSocilaAccount(HashMap<String, String> meMap) {
    if (meMap.size() > 0) {
      meMap.put("_method", "put");
    }
    HandelCalls.getInstance(this).call(DataEnum.updateProfile.name(), meMap, true, this);
  }

  public void updateLevel() {

    HashMap<String, String> meMap = new HashMap<String, String>();
    meMap.put("_method", "put");
    meMap.put("level", level + "");
    meMap.put("transactionId", transactionId);
    HandelCalls.getInstance(this).call(DataEnum.updateProfile.name(), meMap, true, this);

  }

  private void initClick() {
    findViewById(R.id.imgSearch).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // DialogInfoApp.showDialogSearch(HomeActivity.this);
      }
    });
    findViewById(R.id.tvList).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        linearMap.setVisibility(View.GONE);
        LinearList.setVisibility(View.VISIBLE);
      }
    });
    findViewById(R.id.tvMap).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        linearMap.setVisibility(View.VISIBLE);
        LinearList.setVisibility(View.GONE);
      }
    });
  }


  /**
   * Manipulates the map once available. This callback is triggered when the map is ready to be
   * used. This is where we can add markers or lines, add listeners or move the camera. In this
   * case, we just add a marker near Sydney, Australia. If Google Play services is not installed on
   * the device, the user will be prompted to install it inside the SupportMapFragment. This method
   * will only be triggered once the user has installed Google Play services and returned to the
   * app.
   */
  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    // Add a marker in Sydney and move the camera
    LatLng sydney = new LatLng(-34, 151);
    mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    getMenuInflater().inflate(R.menu.main_menu, menu);
    MenuItem item = menu.findItem(R.id.main_all_search);
    String isEnableSearch = PrefsUtil.with(this).get(DataEnum.isFreeSearch.name(), "false");
    if (isEnableSearch.equals("true")) {
      item.setVisible(true);
    }
    // item.setVisible(true);
    return super.onCreateOptionsMenu(menu);
  }

  private void sendToStart() {
    PrefsUtil.with(this).clearAll().apply();
    Intent startIntent = new Intent(this, StartActivity.class);
    startActivity(startIntent);
    finish();

  }
  private void initAddMob() {
    AdView mAdView = findViewById(R.id.adView);
    HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_home));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    super.onOptionsItemSelected(item);

    if (item.getItemId() == R.id.main_logout_btn) {

      sendToStart();

    } else if (item.getItemId() == R.id.main_settings_btn) {

      Intent settingsIntent = new Intent(this, UpdateProfileActivity.class);
      startActivity(settingsIntent);

    } else if (item.getItemId() == R.id.main_all_btn) {
      DialogInfoApp.getInstance(HomeActivity.this).showDialogSocial(this);

    } else if (item.getItemId() == R.id.main_all_search) {
      DialogInfoApp.getInstance(HomeActivity.this).showDialogSearch(this);

    } else if (item.getItemId() == R.id.main_change_lang) {
      HelpMe.getInstance(HomeActivity.this).initLang();
      // recreate();
      startActivity(new Intent(this, HomeActivity.class));
      finish();

    } else if (item.getItemId() == R.id.main_all_fav) {

      startActivity(new Intent(this, Fav.class));
     // finish();

    } else if (item.getItemId() == R.id.main_share_app) {
      Intent shareIntent = new Intent(Intent.ACTION_SEND);
      shareIntent.setType("text/plain");
      shareIntent.putExtra(Intent.EXTRA_TEXT,
          "http://play.google.com/store/apps/details?id=" + getApplicationContext()
              .getPackageName());
      startActivity(Intent.createChooser(shareIntent, "Choose app"));

      Log.e("upgrade", "accounts");

    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
    Log.e("next", "jjjj");
    if (nextPage != null) {
      //Log.e("next",nextPage);
      HashMap<String, String> meMap = new HashMap<String, String>();
      meMap.put(DataEnum.call_get_profile_next_page.name(), nextPage);

      HandelCalls.getInstance(this)
          .call(DataEnum.call_get_profile_next_page.name(), meMap, false, this);

      //  HashMap<String, String> meMap = new HashMap<String, String>();
      // meMap.put(Constant.apiKey, Constant.apiValue);

    } else {
      mRecycler.hideMoreProgress();
    }

  }

  @Override
  public void onResponseSuccess(String flag, Object o) {
    //===========call get all users====================
    Log.e("respnse", "sucess");
    if (flag.equals(DataEnum.callAllProfiles.name())) {
      Log.e("respnse", DataEnum.callAllProfiles.name());
      ModelUsers modelUsers = (ModelUsers) o;
      if (modelUsers.getStatus().getStatus()) {
        Log.e("respnse", DataEnum.callAllProfiles.name());
        nextPage = modelUsers.getPagination().getNext_page_url();

        mAdapter = new AdapterUsers(modelUsers.getData(), HomeActivity.this);
        mRecycler.setAdapter(mAdapter);
      } else {
        List<Data> v = new ArrayList<>();
        mAdapter = new AdapterUsers(v, HomeActivity.this);
        mRecycler.setAdapter(mAdapter);
      }

    }
    //=========end cal get all users===================================//
    else if (flag.equals(DataEnum.call_get_profile_next_page.name())) {
      ModelUsers modelUsers = (ModelUsers) o;
      nextPage = modelUsers.getPagination().getNext_page_url();
      mAdapter.addAll(modelUsers.getData());
    }
//======================================update profile===========//
    else if (flag.equals(DataEnum.updateProfile.name())) {
      Profile profile = (Profile) o;
      if (profile.getStatus().getStatus()) {
        TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG,
            TastyToast.SUCCESS);
        PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();
        initSpinierLib();

      } else {
        TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG,
            TastyToast.ERROR);
      }
    }
    //=======================//
    else if (flag.equals(DataEnum.call_get_braintree_token.name())) {
      String token_braintree = o.toString();

      onBraintreeSubmit(token_braintree);
    } else if (flag.equals(DataEnum.call_get_braintree_check_out.name())) {
      // callRegister ();
      transactionId = o.toString();
      updateLevel();
    }

    //=========================//
  }

  @Override
  public void onNoContent(String flag, int code) {

  }

  @Override
  public void onResponseSuccess(String flag, Object o, int position) {

  }

  @Override
  public void onBadRequest(String flag, Object o) {

  }

  public void onBraintreeSubmit(String token_braintree) {
    DropInRequest dropInRequest = new DropInRequest()
        .clientToken(token_braintree).disablePayPal();
    startActivityForResult(dropInRequest.getIntent(this), BRAINTREE_REQUEST_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == BRAINTREE_REQUEST_CODE) {
      if (resultCode == Activity.RESULT_OK) {
        DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
        String paymentNonce = result.getPaymentMethodNonce().getNonce();
        com.game.of.riches.model.profile.Data profile = (com.game.of.riches.model.profile.Data) PrefsUtil
            .with(this).get(DataEnum.shProfile.name(), com.game.of.riches.model.profile.Data.class);

        HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("nonce", paymentNonce);
        //============================================//
        String cuerrentLevel = profile.getLevel();
        String ValueofCurrentLevel = HelpMe.getInstance(HomeActivity.this).initListLevelsValues()
            .get(Integer.parseInt(cuerrentLevel));
        String ValueofCurrentNewLevel = HelpMe.getInstance(HomeActivity.this).initListLevelsValues()
            .get(level);
        double costUpdate =
            Integer.parseInt(ValueofCurrentNewLevel) - Integer.parseInt(ValueofCurrentLevel);

        double AmountbfrConvert = costUpdate / 1.3;
        String sendAmount = String.format("%.2f", AmountbfrConvert);
        Log.e("amountsend", sendAmount);
        meMap.put("amount", sendAmount);

        ///======================================//

        HandelCalls.getInstance(this)
            .call(DataEnum.call_get_braintree_check_out.name(), meMap, true, this);
        // use the result to update your UI and send the payment method nonce to your server
      } else if (resultCode == Activity.RESULT_CANCELED) {
        // the user canceled
      } else {
        // handle errors here, an exception may be available in
        Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
        DialogInfoApp.showDialogInfo(HomeActivity.this, error.getMessage());
      }
    }
  }


/*
    @OnClick({R.id.imgFB, R.id.imgWhats})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFB:
                SocialChat.FB(HomeActivity.this, "mohamed.algazzar.716");
                break;
            case R.id.imgWhats:
                SocialChat.email(HomeActivity.this, "eng@gmail.com");
                break;

        }

    }
*/

}
