package com.game.of.riches.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.game.of.riches.R;
import com.game.of.riches.adapter.SimpleListAdapter;
import com.game.of.riches.dialog.DialogInfoApp;
import com.game.of.riches.interfaces.HandleRetrofitResp;
import com.game.of.riches.model.profile.Profile;
import com.game.of.riches.retrofitconfig.HandelCalls;
import com.game.of.riches.utlitites.Constant;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.LocaleHelper;
import com.game.of.riches.utlitites.PrefsUtil;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.IdentityProviders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.sdsmdg.tastytoast.TastyToast;
import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.IStatusListener;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class RegisterActivity extends BaseActivity implements ValidationListener, HandleRetrofitResp,
    PurchasesUpdatedListener, ConnectionCallbacks, OnConnectionFailedListener {
    public static final int PAYPAL_REQUEST_CODE = 123;
    private BillingClient mBillingClient;
    String token_braintree = "";
    private static final int BRAINTREE_REQUEST_CODE = 4949;
    // regionPaypal Configuration Object
private int result_email=324;
    //endregion
    //region field
    @NotEmpty

    @BindView(R.id.reg_password)
    EditText regPassword;

    @BindView(R.id.tvDescreption)
    LinearLayout tvDescreption;
    // @BindView(R.id.SearchableSpinner)
    // gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner SearchableSpinner;
    @Email
    @BindView(R.id.register_email)
    EditText registerEmail;
    @BindView(R.id.ch_temp_account)
    CheckBox chTempAccount;
    @BindView(R.id.reg_create_btn)
    Button regCreateBtn;

    @OnClick(R.id.reg_create_btn)
    public void onViewClicked() {
        validator.validate();


    }
    @OnClick(R.id.tvLogin)
    public void onViewClickedtvLogin() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));


    }


    //endregion
    private Toolbar mToolbar;
    private SearchableSpinner mSearchableSpinner;
    private SimpleListAdapter mSimpleArrayListAdapter;
    public ArrayList<String> mStrings = new ArrayList<>();
    private String level = "-1";
    private String transactionId = "";
    private Validator validator;
    @NotEmpty
    @BindView(R.id.profile_fullname)
    EditText profile_fullname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        if (!PrefsUtil.with(RegisterActivity.this).get(DataEnum.isFreeUnabled.name(), "true").equals("true")) {
            chTempAccount.setVisibility(View.GONE);
        }
        chTempAccount.setVisibility(View.GONE);
        initToolBar();
        requestEmailHint();

        // initSpiner();
        // initListValues();
        initSpinierLib();
        initValidator();

initAddMob();
    }

    private void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.register));
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initAddMob() {
        AdView mAdView = findViewById(R.id.adView);

        HelpMe.initAddMobG(this,mAdView,getString(R.string.mobads_register));
    }


    @Override
    public void onValidationSucceeded() {
        level = "25";
        callRegister();
        /*
        // startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        if (chTempAccount.isChecked()) {
            level = "25";
            callRegister();
        } else {
            if (!level.equals("-1")) {
                isServiceInAppAvailable();
              //  HandelCalls.getInstance(this).call(DataEnum.call_get_braintree_token.name(), null, true, this);
            } else {
                TastyToast.makeText(RegisterActivity.this, getString(R.string.validate_choose_level), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }
*/

    }
    @Override
    protected void attachBaseContext(Context cnt) {
        super.attachBaseContext(LocaleHelper.onAttach(cnt));
        Log.e("onbase","base0");
    }
    private void isServiceInAppAvailable() {
      //  Log.e("code", level + "");
        mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingResponse.OK) {
                    BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                        .setSku(level)
                        .setType(SkuType.INAPP) // SkuType.SUB for subscription
                        .build();
                    int responseCode = mBillingClient.launchBillingFlow(RegisterActivity.this, flowParams);

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }
    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingResponse.OK
            && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (responseCode == BillingResponse.USER_CANCELED) {
           // DialogInfoApp.showDialogInfo(this, "cancelled");
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            DialogInfoApp.showDialogInfo(this, "error");
            // Handle any other error codes.
        }
    }
    private void handlePurchase(Purchase purchase) {
        transactionId=purchase.getOriginalJson();
        callRegister();
       // TastyToast.makeText(this, purchase.getOriginalJson(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
      //  DialogInfoApp.showDialogInfo(this, purchase.getOriginalJson());

    }

    public void callRegister() {
        HashMap<String, String> meMap = new HashMap<String, String>();
        meMap.put("email", registerEmail.getText().toString());
        //  meMap.put("full_name", "");
        meMap.put("full_name", profile_fullname.getText().toString());
        PrefsUtil.with(this).add("fullName",profile_fullname.getText().toString()).apply();
        meMap.put("password", regPassword.getText().toString());
        meMap.put("level", level);
        meMap.put("transactionId", transactionId);

        HandelCalls.getInstance(RegisterActivity.this).call(DataEnum.registerCall.name(), meMap, true, this);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        if (flag.equals(DataEnum.call_get_braintree_token.name())) {
            token_braintree = o.toString();

            onBraintreeSubmit();
        } else if (flag.equals(DataEnum.call_get_braintree_check_out.name())) {
            transactionId = o.toString();
            callRegister();
        } else {
            Profile profile = (Profile) o;
            if (profile.getStatus().getStatus()) {
                profile.getData().setEmail(registerEmail.getText().toString());
                profile.getData().setFull_name(PrefsUtil.with(RegisterActivity.this).get("fullName",""));
                PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();
                PrefsUtil.with(this).add(DataEnum.AuthToken.name(), profile.getData().getToken()).apply();
                // Data s= (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(),Data.class);
                // Log.e("my nmae",profile.getData().getFull_name());
                Intent i = new Intent(RegisterActivity.this, UpdateProfileActivity.class);
                i.putExtra("fromregister", "true");
                startActivity(i);
                finish();
            } else {
                TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }


    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }

    private void initSpinierLib() {
        ArrayList<String> mStringsOrig = HelpMe.getInstance(RegisterActivity.this).initListLevels();
         // Collections.reverse(mStrings);
        mStrings=new ArrayList<String>();
        for (int i = 0; i <mStringsOrig.size() ; i++) {
         mStrings.add(mStringsOrig.get(i));
        }
        Collections.reverse(mStrings);
        mSimpleArrayListAdapter = new SimpleListAdapter(this, mStrings);
        mSearchableSpinner = (SearchableSpinner) findViewById(R.id.SearchableSpinner);
        mSearchableSpinner.setVisibility(View.VISIBLE);
        mSearchableSpinner.setAdapter(mSimpleArrayListAdapter);
        mSearchableSpinner.setOnItemSelectedListener(mOnItemSelectedListener);
        mSearchableSpinner.setStatusListener(new IStatusListener() {
            @Override
            public void spinnerIsOpening() {

            }

            @Override
            public void spinnerIsClosing() {

            }
        });
    }

    private OnItemSelectedListener mOnItemSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(View view, int position, long id) {
            Log.e("poos",position+"");
            if(position>0){
                String levelName=mStrings.get(position-1);
                ArrayList<String> arrString = HelpMe.getInstance(RegisterActivity.this)
                    .initListLevels();
                position  =   arrString.indexOf(levelName);
               // position = position - 1;
                level = position + "";
                Log.e("level",level);
                if(position<Constant.maxAvailableLevel){
                    level="-1";
                    String levelStr=HelpMe.getInstance(RegisterActivity.this).mStrings.get(position)+" ";
                    DialogInfoApp.showDialogInfo(RegisterActivity.this, levelStr+getString(R.string.dialog_no_available_level));
                }
            }

           /* Toast.makeText(RegisterActivity.this, "Item on position " + position + " : " + mSimpleArrayListAdapter.getItem(position) + " Selected", Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onNothingSelected() {
            //Log.e("poos","none");
           // Toast.makeText(RegisterActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
        }
    };




   /* private void initSpiner() {
        List<String> SpinnerList = new ArrayList<String>();
        for (int i = 0; i <10 ; i++) {
            SpinnerList.add(i+"mm");

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner, SpinnerList);
        spLevel.setAdapter(adapter);

        spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("item", "items");

                // initSelectedDocType(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BRAINTREE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String paymentNonce = result.getPaymentMethodNonce().getNonce();
                HashMap<String, String> meMap = new HashMap<String, String>();
                meMap.put("nonce", paymentNonce);
                String StrAmountbfrConvert = HelpMe.getInstance(RegisterActivity.this).initListLevelsValues().get(Integer.parseInt(level));
                double AmountbfrConvert = Integer.parseInt(StrAmountbfrConvert) / 1.3;
                String sendAmount=String.format("%.2f", AmountbfrConvert);
                Log.e("amountsend",sendAmount);
                meMap.put("amount",sendAmount);
                //   meMap.put("amount","1");
                HandelCalls.getInstance(this).call(DataEnum.call_get_braintree_check_out.name(), meMap, true, this);
                // use the result to update your UI and send the payment method nonce to your server
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                //  TastyToast.makeText(RegisterActivity.this,error.getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                Log.e("messageerroreis", error.getMessage());
                DialogInfoApp.showDialogInfo(RegisterActivity.this, error.getMessage());
            }
        }
        else if(requestCode==result_email){
            if (resultCode == RESULT_OK) {
              Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                //cachEmailName(credential);
                registerEmail.setText(credential.getId());
                PrefsUtil.with(this).add("fullName",credential.getName()).apply();
                profile_fullname.setText(credential.getName());
          //  Log.e("idcreee", credential.getName() +"-"+ credential.getFamilyName() + credential.getGivenName());
            }
        }
    }

    public void onBraintreeSubmit() {
        /*
        PayPalRequest request = new PayPalRequest("1")
                .currencyCode("USD")
                .intent(PayPalRequest.INTENT_AUTHORIZE);
                */
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(token_braintree).disablePayPal();
        startActivityForResult(dropInRequest.getIntent(this), BRAINTREE_REQUEST_CODE);
    }
   private void requestEmailHint() {

        HintRequest hintRequest = new HintRequest.Builder()
            .setEmailAddressIdentifierSupported(true)
            //.setPhoneNumberIdentifierSupported(true)
            .setAccountTypes(IdentityProviders.GOOGLE)
            .build();
       GoogleApiClient apiClient = apiClient = new GoogleApiClient.Builder(this)
           .addConnectionCallbacks(this)
           .enableAutoManage(this, this)
           .addApi(Auth.CREDENTIALS_API)
           .build();
       PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(apiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), result_email, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e("idcreee", e.getMessage());
            e.printStackTrace();
        }
  }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
