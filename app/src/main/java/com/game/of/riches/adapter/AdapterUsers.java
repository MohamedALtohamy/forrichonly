package com.game.of.riches.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.game.of.riches.R;
import com.game.of.riches.activity.HomeActivity;
import com.game.of.riches.dialog.DialogInfoApp;
import com.game.of.riches.dialog.SocialChat;
import com.game.of.riches.model.users.Data;
import com.game.of.riches.utlitites.DataEnum;
import com.game.of.riches.utlitites.HelpMe;
import com.game.of.riches.utlitites.PrefsUtil;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 5/3/2016.
 */
public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.ViewHolder> {


  public List<Data> data;

  private HomeActivity mContext;


  public AdapterUsers(List<Data> data, HomeActivity mContext) {
    this.data = data;
    this.mContext = mContext;
  }


  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.search_result, parent, false);
    //  ButterKnife.bind(mContext, view);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {

    // holder.tvDoctoreName.setText(data.get(position).getFirstName() + " " + data.get(position).getLastName());
    Data profile = data.get(position);
    String userId = data.get(position).getId();
    String stredUserId = PrefsUtil.with(mContext).get(userId, "");
    if (stredUserId.equals(userId)) {
      holder.imgHeart.setImageResource(R.drawable.ic_like);
    } else {
      holder.imgHeart.setImageResource(R.drawable.ic_heart_outline);
    }
    holder.tvTitle.setText(isUnkonwn(profile.getStatus_note()));
    holder.tvFullName.setText(isUnkonwn(profile.getFull_name()));

    holder.tvCity.setText(isUnkonwnCountry(profile.getCountry_code()));
    String color="l"+data.get(position).getLevel();
    int resId = mContext.getResources().getIdentifier(color, "color", mContext.getPackageName());
    holder.imgLevel.setColorFilter(mContext.getResources().getColor(resId));
    //ImageViewCompat.setImageTintList(holder.imgLevel, mContext.getResources().getColorStateList(R.color.mdtp_red));
    //holder.imgLevel.setColorFilter(ContextCompat.getColor(mContext, R.color.pdlg_color_red), android.graphics.PorterDuff.Mode.MULTIPLY);
  /*  String age="";
    if(profile.getDate_of_birth()!=null){
      if(!profile.getDate_of_birth().equals("")){
      age=  HelpMe.getDiffYears(profile.getDate_of_birth())+"";

      }
    }*/

    holder.tvGender.setText(genderType(profile.getGender()));
    String level = HelpMe.getInstance(mContext).initListLevels()
        .get(Integer.parseInt(data.get(position).getLevel()));
    holder.tvLevel.setText(level);
    Picasso.with(mContext)
        .load(isUnkonwn(profile.getAvatar_link()))
        .placeholder(R.drawable.splash).into(holder.img);
    if (!profile.getFacebook_acc().equals("")) {
      if (isAllowedLevel(Integer.parseInt(profile.getLevel()))) {
        holder.imgFB.setVisibility(View.VISIBLE);
      }
            /*

            holder.imgFB.setEnabled(true);
            holder.nonFb.setVisibility(View.GONE);
            */
    } else {
      holder.imgFB.setVisibility(View.GONE);
           /*
            holder.imgFB.setEnabled(false);
           holder.nonFb.setVisibility(View.GONE);
           */
    }
    if (!profile.getWhatsapp_acc().equals("")) {

      holder.imgWhats.setVisibility(View.VISIBLE);
            /*
            holder.imgWhats.setEnabled(true);
            holder.nonFb.setVisibility(View.GONE);
            // holder.imgWhats.setImageResource(R.drawable.ic_whatsappun);
          //  holder.nonWhats.setVisibility(View.GONE);
          */
    } else {
      holder.imgWhats.setVisibility(View.GONE);
            /*
            holder.imgWhats.setEnabled(false);
            holder.nonWhats.setVisibility(View.GONE);
            */
    }
    if (!profile.getTelegram_acc().equals("")) {
      holder.imgTelegram.setVisibility(View.VISIBLE);
            /*
            holder.imgTelegram.setAlpha((float) 1);
            holder.imgTelegram.setEnabled(true);
            holder.nonTel.setVisibility(View.GONE);
            */
    } else {
      holder.imgTelegram.setVisibility(View.GONE);
            /*
            holder.imgTelegram.setEnabled(false);
            holder.nonTel.setVisibility(View.GONE);
            */
    }
    if (!profile.getInstagram_acc().equals("")) {
      holder.imginstgram.setVisibility(View.VISIBLE);
            /*
            holder.imginstgram.setAlpha((float) 1);
            holder.imginstgram.setEnabled(true);
            holder.nonInst.setVisibility(View.GONE);
            */
    } else {
      holder.imginstgram.setVisibility(View.GONE);
            /*
            holder.imginstgram.setEnabled(false);
            holder.nonInst.setVisibility(View.GONE);
            */
    }
  }

  private String isUnkonwn(String value) {
    if (value.equals("")) {
      // return mContext.getString(R.string.unknown);
    return "";
      //
    } else {
      return value;
    }
  }
  private String isUnkonwnCountry(String value) {
    if (value.equals("")) {
       return mContext.getString(R.string.unknown);
     /// return "";
      //
    } else {
      return value;
    }
  }

  private String genderType(String value) {
    if (value.equals("2")) {
      return mContext.getString(R.string.female);
    } else if (value.equals("1")) {
      return mContext.getString(R.string.male);
    } else {
      return "Male";
    }
  }

  private boolean isAllowedLevel(int CurrentRowLevel) {

    com.game.of.riches.model.profile.Data profile = (com.game.of.riches.model.profile.Data) PrefsUtil
        .with(mContext).get(DataEnum.shProfile.name(), com.game.of.riches.model.profile.Data.class);
    int currentUserLevel = Integer.parseInt(profile.getLevel());

    return CurrentRowLevel >= currentUserLevel;
  }

  private void showDialogUpdateLevel(String level) {
    String strlevel = HelpMe.getInstance(mContext).mStrings.get(Integer.parseInt(level));
    DialogInfoApp.showDialogInfoUpdateLevel(mContext,
        mContext.getString(R.string.updtae_to_class_) + " " + strlevel, level);
  }

  @Override
  public int getItemCount() {
    return data.size();
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  public void add(String string) {
    insert(string, data.size());
  }

  public void insert(String string, int position) {
    //data.add(position, string);
    notifyItemInserted(position);
  }

  public void remove(int position) {
    data.remove(position);
    notifyItemRemoved(position);
  }

  public void clear() {
    int size = data.size();
    data.clear();
    notifyItemRangeRemoved(0, size);
  }

  public void addAll(List<Data> items) {
    int startIndex = data.size();
    data.addAll(items);
    notifyItemRangeInserted(startIndex, items.size());
  }


  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    ImageView img;
    TextView tvTitle, tvFullName;

    TextView tvCity;
    ImageView imggender;
    TextView tvGender;
    TextView tvLevel;
    ImageView imgFB;
    ImageView imgTelegram;
    ImageView imgWhats;
    ImageView imgEmail;
    ImageView imginstgram;
    CardView cardView;
    TextView nonFb, nonWhats, nonTel, nonInst;
    ImageView imgHeart,imgLevel;

    public ViewHolder(View itemView) {
      super(itemView);

      itemView.setOnClickListener(this);
/*
            nonFb = itemView.findViewById(R.id.tvNoneFB);
            nonInst = itemView.findViewById(R.id.tvNoneInst);
            nonTel = itemView.findViewById(R.id.tvNoneTel);
            nonWhats = itemView.findViewById(R.id.tvNoneWhats);
*/
      img = itemView.findViewById(R.id.img);
      imgLevel = itemView.findViewById(R.id.imgLevel);
      imgHeart = itemView.findViewById(R.id.imgHeart);
      imgHeart.setOnClickListener(this);
      tvTitle = itemView.findViewById(R.id.tvTitle);
      tvFullName = itemView.findViewById(R.id.tvFullName);
      tvTitle.setOnClickListener(this);
      tvCity = itemView.findViewById(R.id.tvCity);
      tvGender = itemView.findViewById(R.id.tvGender);
      tvLevel = itemView.findViewById(R.id.tvLevel);

      imgFB = itemView.findViewById(R.id.imgFB);
      imgTelegram = itemView.findViewById(R.id.imgTelegram);
      imgWhats = itemView.findViewById(R.id.imgWhats);
      imgEmail = itemView.findViewById(R.id.imgEmail);
      imginstgram = itemView.findViewById(R.id.imginstgram);
      imgFB.setOnClickListener(this);
      imgTelegram.setOnClickListener(this);
      imgWhats.setOnClickListener(this);
      imgEmail.setOnClickListener(this);
      imginstgram.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

      switch (v.getId()) {
        case R.id.tvTitle:
          if (!data.get(getAdapterPosition()).getStatus_note().equals("")) {
            DialogInfoApp.showDialogInfo(mContext, data.get(getAdapterPosition()).getStatus_note());
          }
          break;
        case R.id.imgHeart:
          String userId = data.get(getAdapterPosition()).getId();
          String stredUserId = PrefsUtil.with(mContext).get(userId, "");
          List<Data> dataStored = PrefsUtil.with(mContext).get(Data.class, "fav");
          if (stredUserId.equals(userId)) {
            PrefsUtil.with(mContext).remove(userId).apply();
            if (dataStored != null) {
              if(dataStored.size()>0){
                for (int i = 0; i < dataStored.size(); i++) {
                  if(dataStored.get(i).getId().equals(userId)){
                    dataStored.remove(i);
                    PrefsUtil.with(mContext).add("fav",dataStored).apply();
                    break;
                  }
                }
              }

            }
          } else {
            PrefsUtil.with(mContext).add(userId, userId).apply();
            if (dataStored != null) {
              dataStored.add(data.get(getAdapterPosition()));
              PrefsUtil.with(mContext).add("fav",dataStored).apply();
              Log.e("leeee",dataStored.size()+"");
            } else {
              dataStored = new ArrayList<Data>();
              dataStored.add(data.get(getAdapterPosition()));
              PrefsUtil.with(mContext).add("fav",dataStored).apply();

            }
          }
          notifyItemChanged(getAdapterPosition());
          break;
        case R.id.imgFB:
          if (isAllowedLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()))) {
            SocialChat.FB(mContext, data.get(getAdapterPosition()).getFacebook_acc());
          } else {
            showDialogUpdateLevel(data.get(getAdapterPosition()).getLevel());
            //  String level=HelpMe.getInstance(mContext).mLevelsValues.get(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
            // showDialogUpdateLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
          }

          break;
        case R.id.imgTelegram:

          if (isAllowedLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()))) {
            SocialChat.Telegrame(mContext, data.get(getAdapterPosition()).getTelegram_acc());
          } else {
            showDialogUpdateLevel(data.get(getAdapterPosition()).getLevel());
            //  String level=HelpMe.getInstance(mContext).mLevelsValues.get(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
            // showDialogUpdateLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
          }
          break;
        case R.id.imgWhats:

          if (isAllowedLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()))) {
            SocialChat.whatsApp(mContext, data.get(getAdapterPosition()).getWhatsapp_acc());
          } else {
            showDialogUpdateLevel(data.get(getAdapterPosition()).getLevel());
            //  String level=HelpMe.getInstance(mContext).mLevelsValues.get(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
            // showDialogUpdateLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()));
          }
          break;
        case R.id.imgEmail:
          if (isAllowedLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()))) {
            SocialChat.email(mContext, data.get(getAdapterPosition()).getEmail());
          } else {

            showDialogUpdateLevel(data.get(getAdapterPosition()).getLevel());
          }

          break;
        case R.id.imginstgram:

          if (isAllowedLevel(Integer.parseInt(data.get(getAdapterPosition()).getLevel()))) {
            SocialChat.instgram(mContext, data.get(getAdapterPosition()).getInstagram_acc());
          } else {

            showDialogUpdateLevel(data.get(getAdapterPosition()).getLevel());
          }
          break;
        default:
          if (!data.get(getAdapterPosition()).getStatus_note().equals("")) {
            DialogInfoApp.showDialogInfo(mContext, data.get(getAdapterPosition()).getStatus_note());
          }

          break;
      }
    }

  }
}
